#!user/bin/tclsh

#gcd function that recive two variables and return result
proc gcd {a b} {
    if {$b==0} {return $a} else {gcd $b [expr {$a%$b}]}
}

#fulction read from input file and store/return  pairs of variables in $lines, separated with "\n"
proc read_from_file {path} {
    set input [open $path]
    set content [read $input]
    close $input
    set lines [split $content "\n"]
    return $lines
}

#fulction write results in output file sparated with "\n"
proc write_in_file {path result} {
   set is [open $path w]
        puts $is $result
   close $is
}

#recive lines of necssary parameters and return result within $result separated whith "\n"
proc calculate {lines} {
    set result []
    foreach i $lines {
        set pair [split $i " "]
        if { [lindex $pair 0] != {} && [lindex $pair 1] != {} } {
			append result [ gcd [lindex $pair 0] [lindex $pair 1] ]
            append result "\n"
		} 
    }
    return $result
}

proc main {} {
    set input_content [read_from_file "test1.input" ]
    set results [ calculate  $input_content  ]
    write_in_file "test1.output" $results
}
main
