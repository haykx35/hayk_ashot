#!user/bin/tclsh
set source_filename "gcd.tcl"
source $source_filename

#comparing and return result in *.result file

proc compare1 {input out expected result} {
    set res [open $result w]    
    for {set i 0} {$i < [llength $out]} {incr i} {
        if { [lindex $out $i] == [lindex $expected $i] } {
        puts $res " ([lindex $input $i]) -> [lindex $out $i] | [lindex $expected $i] ///PASS///"
        } else {
        puts $res " ([lindex $input $i]) -> [lindex $out $i] | [lindex $expected $i] ///FAIL///"
        }
    }
    close $res
}

proc main {} {
    #menak comnparena vor chem grel
   set results [ calculate [ read_from_file "test1.input" ] ]
   write_in_file "test1.output" $results
   set expected [read_from_file "test1.expected"]
   set input [read_from_file "test1.input"]
   puts [llength $input]
   puts [llength $results]
   puts [llength $expected]
   compare1 $input $results $expected "test1.result"
}
main
