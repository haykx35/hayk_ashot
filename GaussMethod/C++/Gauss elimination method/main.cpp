#include <iostream>
#include <vector>
#include <fstream>
#include "Matrix.h"
#include "FileNotFoundException.h"
//#include "InvalidInputFileException.h"
//#include "NoSpecificSolution.h"


Matrix
read_from_file(const std::string &filename) {
    std::vector<std::vector<double>> vectors;
    int numberOfEquations;
    std::ifstream file;
    file.open(filename);
    if (file.fail()) {
        ///throw FileNotFoundException("File not found");
    }
    file >> numberOfEquations;
    if (numberOfEquations < 0) {
        //throw InvalidInputFileException("Invalid input");
    }
    vectors.resize(numberOfEquations);
    for (auto &vector: vectors) {
        for (int j = 0; j < numberOfEquations + 1; ++j) {
            double val;
            file >> val;
            //invalid input value can be handled using std::optional (C++20)
            vector.push_back(val);
        }
    }
    Matrix matrix(vectors);
    file.close();
    return matrix;
}

void
write_into_file(std::vector<double> &&result, const std::string &filename) {
    std::ofstream file;
    file.open(filename);
    for (int i = 0; i < result.size(); ++i) {
        file << "x[" << i + 1 << "] = " << result[i] << "\n";
    }

    file.close();
}

Matrix
transform_to_upper_triangular(Matrix matrix, int lineIndex = 1) {
    if (lineIndex == matrix.count_of_rows()) {
        return matrix;
    }
    size_t size = matrix.count_of_rows();

    for (int i = lineIndex; i < size; ++i) {
        double coefficient = matrix[i][lineIndex - 1] / matrix[lineIndex - 1][lineIndex - 1];
        std::vector<double> tempRow = matrix.get_row(lineIndex - 1);
        std::vector<double> newRow = matrix.get_row(i);
        for (int j = 0; j < tempRow.size(); ++j) {
            newRow[j] -= coefficient * tempRow[j];
        }
        matrix.set_row(newRow, i);
    }
    ++lineIndex;
    return transform_to_upper_triangular(matrix, lineIndex);
}

std::vector<double>
backward_substitution(const Matrix &matrix) {
    double freeVariable = matrix[matrix.count_of_rows() - 1][matrix.count_of_columns() - 1];
    double coefficient = matrix[matrix.count_of_rows() - 1][matrix.count_of_columns() - 2];

    if (coefficient == 0 && freeVariable == 0) {
        throw NoSpecificSolution("There is a set of solutions");
    }
    if (coefficient == 0) {
        throw NoSpecificSolution("There is no solution");
    }

    std::vector<double> solution;
    int size = (int) matrix.count_of_rows();

    for (int i = size - 1; i >= 0; --i) {
        double sum = 0;
        for (int j = 0; j < solution.size(); ++j) {
            sum += matrix[i][size - j - 1] * solution[j];
        }
        solution.push_back((matrix[i][size] - sum) / matrix[i][i]);
    }
    std::reverse(solution.begin(), solution.end());
    return solution;
}


std::vector<double>
solve(const Matrix &matrix) {
    return backward_substitution(transform_to_upper_triangular(matrix));
}

int main() {
/*    std::vector<double> sol = solve(Matrix({
                                                   {2, 1, 4, 1},
                                                   {2, 7, 5, 4},
                                                   {6, 5, 4, 79}
                                           }));


    //solution 17.5 2 -9
    for (auto k: sol) {
        std::cout << k << ' ';
    }
    std::cout << std::endl;

    std::vector<double> s = solve(Matrix({
                                                 {1,  2,  3, 4,  5},
                                                 {6,  7,  8, 9,  10},
                                                 {11, 12, 3, 1,  15},
                                                 {16, 17, 7, 19, 20}
                                         }));
    //solution -3 4 0 0
    for (auto k: s) {
        std::cout << k << ' ';
    }
    std::cout << std::endl;*/
    write_into_file(solve(read_from_file("../input.txt")), "../output.txt");


    return 0;
}
