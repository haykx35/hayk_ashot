//
// Created by User on 7/22/2022.
//

#ifndef GAUSS_ELIMINATION_METHOD_FILENOTFOUNDEXCEPTION_H
#define GAUSS_ELIMINATION_METHOD_FILENOTFOUNDEXCEPTION_H

#include <exception>

class FileNotFoundException: public std::exception{
public:
    explicit FileNotFoundException(const char *message) : std::exception(message) {}
};

#endif //GAUSS_ELIMINATION_METHOD_FILENOTFOUNDEXCEPTION_H
