//
// Created by User on 7/22/2022.
//

#ifndef GAUSS_ELIMINATION_METHOD_INVALIDINPUTFILEEXCEPTION_H
#define GAUSS_ELIMINATION_METHOD_INVALIDINPUTFILEEXCEPTION_H

#include <exception>

class InvalidInputFileException : public std::exception {
public:
    explicit InvalidInputFileException(const char *message) : std::exception(message) {}
};

#endif //GAUSS_ELIMINATION_METHOD_INVALIDINPUTFILEEXCEPTION_H
