#ifndef FILE_H
#define FILE_H

#include <vector>
#include <fstream>

class File{
public:
    static std::vector<std::pair<double, double>> readFromFile(const std::string&);

    static void writeInFile(const std::string &path, const std::vector<std::pair<double, std::string>>&);

    static void compare(const std::string &, const std::string &, const std::string &);
};

#endif //FILE_H
