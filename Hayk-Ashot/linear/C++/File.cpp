#include "File.h"

std::vector<std::pair<double, double>>
File::readFromFile(const std::string &path) {
    std::vector<std::pair<double, double>> pairs;
    std::ifstream file;
    file.open(path);
    while (file.good()) {
        std::pair<double, double> pair;
        file >> pair.first;
        file >> pair.second;
        pairs.push_back(pair);
    }
    file.close();
    return pairs;
}

void
File::writeInFile(const std::string &path, const std::vector<std::pair<double, std::string>> &results) {
    std::ofstream file;
    file.open(path);
    for (int i = 0; i < results.size() - 1; ++i) {
        if (results[i].second.empty()) {
            file << results[i].first << '\n';
        } else {
            file << results[i].second << '\n';
        }
    }

    std::pair<double, std::string> last = results[results.size() - 1];

    if (last.second.empty()) {
        file << last.first;
    } else {
        file << last.second;
    }

    file.close();
}

void
File::compare(const std::string &path1, const std::string &path2, const std::string &path3) {
    std::ifstream output(path1);
    std::ifstream expected(path2);
    std::ofstream result(path3);
    std::string line1;
    std::string line2;
    std::getline(output, line1);
    std::getline(expected, line2);
    int lineNumber = 1;
    int incorrect = 0;
    while (!line1.empty() && !line2.empty()) {
        if (line1 != line2) {
            ++incorrect;
            result << "Line: " << lineNumber << ". Output - " << line1 << ", Expected - " << line2 << '\n';
        }
        line1.clear();
        line2.clear();
        std::getline(output, line1);
        std::getline(expected, line2);
        ++lineNumber;
    }

    result << (lineNumber - 1 - incorrect) / (double) (lineNumber - 1) * 100 << "% of tests was passed";

    output.close();
    expected.close();
    result.close();
}
