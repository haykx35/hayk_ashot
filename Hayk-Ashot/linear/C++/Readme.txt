////A program that calculates solution of a linear equations////

The program reads pairs of doubles(also integers) from input.txt, then calculates linear equation taking input pair
elements as coefficient of equation and
writes them into output.txt. Then program compares program generated results with the expected results
and writes differences in result.txt file and the percentage of passed tests.

To build the program type *make*
To run the program type *./a.out*
To delete generated files after program execution type *make clean*

////Required files for the program to work////
1. input.txt
    1. must contain pairs of numbers
2. expected.txt
    1. must contain expected results

////File class////
File class has 3 static methods that gives ability to read from file, write into file and compare
two files writing result in result.txt

////InfiniteSolutionsException NoSolutionException classes////
Depending on the coefficients of variables, sometimes linear equations have infinitely many solutions or no solution,
in those case program throws InfiniteSolutionsException or NoSolutionException exception respectively.

THANK YOU
