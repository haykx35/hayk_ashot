#include <vector>
#include "File.h"
#include "InfiniteSolutionsException.h"
#include "NoSolutionsException.h"


double linear_equation(double a, double b) {
    if (a == 0 && b == 0) {
        throw InfiniteSolutionsException("X c R");
    }
    if (a == 0) {
        throw NoSolutionsException("X c O");
    }
    return -b / a;
}

std::vector<std::pair<double, std::string>>
calculateLinearEquation(const std::vector<std::pair<double, double>> &pairs) {
    std::vector<std::pair<double, std::string>> results;
    for (auto pair: pairs) {
        std::string state;
        double solution{};
        try {
            solution = linear_equation(pair.first, pair.second);
        }
        catch (const NoSolutionsException &e) {
            state = e.what();
        }
        catch (const InfiniteSolutionsException &e) {
            state = e.what();
        }
        results.emplace_back(solution, state);
    }
    return results;
}


int main() {
    std::vector<std::pair<double, double>> pairs = File::readFromFile("input.txt");
    std::vector<std::pair<double, std::string>> results = calculateLinearEquation(pairs);
    File::writeInFile("output.txt", results);
    File::compare("output.txt", "expected.txt", "result.txt");
    return 0;
}
