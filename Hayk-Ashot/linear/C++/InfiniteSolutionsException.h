#ifndef INFINITESOLUTIONEXCEPTION_H
#define INFINITESOLUTIONEXCEPTION_H

#include <exception>

class InfiniteSolutionsException : public std::exception {
public:
    explicit InfiniteSolutionsException(const char *message) : message(message) {}

    const char *what() const

    noexcept override{
            return message;
    }
private:
    const char *message;
};

#endif //INFINITESOLUTIONEXCEPTION_H
