const fs = require('fs');

function linear_equation(a, b){
    if( a === 0 && b === 0 ){
        return "X c R";
    }
    if(a === 0 && b !== 0){
        return "X c O";
    }
    return -b / a;
}


function read(fileName){
    let data =  fs.readFileSync(fileName, 'utf-8').split('\n');
    data.pop();
    return data;
}

function write(fileName, dataArray){
    fs.writeFileSync(fileName, dataArray.join('\n'));
}


function linearEquationCalculate(inputPath, outputPath){
    let data = read(inputPath);
    let results = [];
    for(let i=0;i<data.length;++i){
        let args = data[i].split(' ');
        results.push(linear_equation(+args[0], +args[1]));
    }
    results.push('');
    write(outputPath, results);
}

function linearEquationTest(outputPath, expectedPath, resultPath){
    let output = read(outputPath);
    let expected = read(expectedPath);
    let result = [];
    let passed = 0;

    for(let i=0; i < output.length; ++i){
        if(output[i]!==expected[i]){
            result.push(`Line ${i+1}: Output - ${output[i]}, Expected - ${expected[i]}`);
        }else{
            ++passed;
        }
    }
    result.push(`${passed/output.length*100}% of tests was passed`);
    write(resultPath, result);
}



function main(){
	linearEquationCalculate('input.txt', 'output.txt');
	linearEquationTest('output.txt', 'expected.txt', 'result.txt');
}

main();




