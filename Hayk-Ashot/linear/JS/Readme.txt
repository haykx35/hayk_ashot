////A program that calculates solution of a linear equations////

The program reads pairs of numbers from input.txt, then calculates linear equation taking input pair
elements as coefficient of equation and
writes them into output.txt. Then program compares program generated results with the expected results
and writes differences in result.txt file and the percentage of passed tests.

To run the program type *make*
To delete generated files after program execution type *make clean*

////Required files for the program to work////
1. input.txt
    1. must contain pairs of numbers
2. expected.txt
    1. must contain expected results

THANK YOU
