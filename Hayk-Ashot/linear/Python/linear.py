#!bin/pyhton3
def linear (a,b):
    if a==0:
        if b==0:
            return "X c R"
        return "X c 0"
    return (-b/a)

def readFromFile(path):
    file = open(path, 'r')
    pairs = []
    for line in file:
        number1 = line.split(' ')[0]
        number2 = line.split(' ')[1]
        pairs.append([int(number1), int(number2)])
    file.close()
    return pairs

def calculate_linear(pairs):
    results = []
    for pair in pairs:
        results.append(linear(pair[0], pair[1]))
    return results

def writeIntoFile(path,results):
    file = open(path, 'w')
    for result in results:
        file.write(str(result) + '\n')
    file.close()

def compare(path1, path2, path3):
    output = open(path1, 'r')
    expected = open(path2, 'r')
    result = open(path3, 'w')
    line_number = 1
    incorrect = 0
    while True:
        line1 = output.readline()
        line2 = expected.readline()
        if not line1 or not line2:
            break
        if line1 != line2:
            incorrect += 1
            result.write("Line: " + str(line_number) + " Output - " + str(line1.split('\n')[0]) + ", Expected - " + str(line2))
        line_number += 1

    result.write(str((line_number - 1 - incorrect) / (line_number - 1) * 100) + "% of tests was passed")
    output.close()
    expected.close()
    result.close()

def main():
    pairs = readFromFile('test1.input')
    results = calculate_linear(pairs)
    writeIntoFile('test1.output', results)
    compare('test1.output', 'test1.expected', 'test1.result')
main()
