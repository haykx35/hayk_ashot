#include <vector>
#include <fstream>

#ifndef FILE_H
#define FILE_H
class File{
public:
    static std::vector<std::pair<int, int>> readFromFile(const std::string&);

    static void writeInFile(const std::string &path, const std::vector<int>&);

    static void compare(const std::string &, const std::string &, const std::string &);
};

#endif //FILE_H
