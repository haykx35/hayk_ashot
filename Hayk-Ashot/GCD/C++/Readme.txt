////A program that calculates the greatest common divisor of two integers using recursive algorithm////

The program reads pairs of integers from input.txt, then calculates their greatest common divisors and
writes them into output.txt. Then program compares program generated results with the expected results
and writes differences in result.txt file and the percentage of passed tests.

To build the program type *make*
To run the program type *./a.out*
To delete generated files after program execution type *make clean*

////Required files for the program to work////
1. input.txt
    1. must contain pairs of numbers
2. expected.txt
    1. must contain expected results

////File class////
File class has 3 static methods that gives ability to read from file, write into file and compare
two files writing result in result.txt

THANK YOU
