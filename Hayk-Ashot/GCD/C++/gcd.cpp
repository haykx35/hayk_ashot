#include <vector>
#include "File.h"


int gcd(int a, int b) {
    return (b == 0) ? a : gcd(b, a % b);
}


std::vector<int> calculateGCD(const std::vector<std::pair<int, int>> &pairs) {
    std::vector<int> results;
    for (auto pair: pairs) {
        results.push_back(gcd(pair.first, pair.second));
    }
    return results;
}



int main() {
    std::vector<std::pair<int,int>> pairs = File::readFromFile("input.txt");
    std::vector<int> results = calculateGCD(pairs);
    File::writeInFile("output.txt",results);
    File::compare("output.txt", "expected.txt", "result.txt");
    return 0;
}
