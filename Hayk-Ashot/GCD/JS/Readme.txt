////A program that calculates the greatest common divisor of two integers using recursive algorithm////

The program reads pairs of integers from input.txt, then calculates their greatest common divisors and
writes them into output.txt. Then program compares program generated results with the expected results
and writes differences in result.txt file and the percentage of passed tests.

To run the program use *make*
To delete generated files after program execution use *make clean*

////Required files for the program to work////
1. input.txt
    1. must contain pairs of numbers
2. expected.txt
    1. must contain expected results

THANK YOU
