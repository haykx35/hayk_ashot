const fs = require('fs');

function gcd(a, b){
    return (b==0) ? a : gcd(b, a % b); 
}


function read(fileName){
    let data =  fs.readFileSync(fileName, 'utf-8').split('\n');
    data.pop();
    return data;
}

function write(fileName, dataArray){
    fs.writeFileSync(fileName, dataArray.join('\n'));
}


function gcdCalculate(inputPath, outputPath){
    let data = read(inputPath);
    let results = [];
    for(let i=0;i<data.length;++i){
        let pair = data[i].split(' ');
        results.push(gcd(+pair[0], +pair[1]));
    }
    results.push('');
    write(outputPath, results);
}

function gcdTest(outputPath, expectedPath, resultPath){
    let output = read(outputPath);
    let expected = read(expectedPath);
    let passed = 0;
    let result = [];
    for(let i=0;i<output.length;++i){
        if(output[i]!==expected[i]){
            result.push(`Line ${i+1}: Output - ${output[i]}, Expected - ${expected[i]}`);
        }else{
            ++passed;
        }
    }
    result.push(`${passed/output.length*100}% of tests was passed`);
    write(resultPath, result);
}



function main(){
	gcdCalculate('input.txt', 'output.txt');
	gcdTest('output.txt', 'expected.txt', 'result.txt');
}

main();



