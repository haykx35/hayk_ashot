import sys
def linear (a,b):
    if a==0:
        if b==0:
            return "X c R"
        return "X c 0"
    return (-b/a)

def discriminate(a,b,c):
       return (b*b-4*a*c)

def quadratic(a,b,c):
    if a==0:
        return linear(b,c)
    d=discriminate(a,b,c)
    if d==0:
        return(-b/(2*a))
    if d>0:
        return [((-b + (pow(d, 0.5)))/(2*a)), ((-b -(pow(d, 0.5)))/(2*a))]
    if d<0:
        return "No real solution"

def readFromFile(path):
    file = open(path, 'r')
    pairs = []
    for line in file:
        number1 = line.split(' ')[0]
        number2 = line.split(' ')[1]
        number3 = line.split(' ')[2]
        pairs.append([int(number1), int(number2), int(number3)])
    file.close()
    return pairs

def calculate_linear(pairs):
    results = []
    for pair in pairs:
        results.append(quadratic(pair[0], pair[1], pair[2]))
    return results

def writeIntoFile(path,results):
    file = open(path, 'w')
    for result in results:
        file.write(str(result) + '\n')
    file.close()

def compare(path1, path2, path3):
    output = open(path1, 'r')
    expected = open(path2, 'r')
    result = open(path3, 'w')
    line_number = 1
    incorrect = 0
    while True:
        line1 = output.readline()
        line2 = expected.readline()
        if not line1 or not line2:
            break
        if line1 != line2:
            incorrect += 1
            result.write("Line: " + str(line_number) + " Output - " + str(line1.split('\n')[0]) + ", Expected - " + str(line2))
        line_number += 1

    result.write(str((line_number - 1 - incorrect) / (line_number - 1) * 100) + "% of tests was passed")
    output.close()
    expected.close()
    result.close()

def main():
    pairs = readFromFile('test1.input')
    results = calculate_linear(pairs)
    writeIntoFile('test1.output', results)
    compare('test1.output', 'test1.expected', 'test1.result')
main()
