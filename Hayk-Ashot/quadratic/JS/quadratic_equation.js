const fs = require('fs');

function linear_equation(a, b){
    if( a === 0 && b === 0 ){
        return "X c R";
    }
    if(a === 0 && b !== 0){
        return "X c O";
    }
    return -b / a;
}

function discriminant(a, b, c){
    return Math.pow(b, 2) - 4 * a * c;
}

function quadratic_equation(a, b, c){
    if(a === 0){
        return linear_equation(b, c);
    }

    let d = discriminant(a, b, c);
    
    if(d >= 0){
        return [(-b + Math.pow(d, 1/2)) / (2 * a),
                (-b - Math.pow(d, 1/2)) / (2 * a)];
    }
    
    d-=2*d;
    let bb = -b / (2 * a);
    d = Math.pow(d, 1/2) / (2 * a);
    d += 'i';
    return [bb + ` + ${d}`, bb + ` - ${d}`];
}




function read(fileName){
    let data =  fs.readFileSync(fileName, 'utf-8').split('\n');
    data.pop();
    return data;
}

function write(fileName, dataArray){
    fs.writeFileSync(fileName, dataArray.join('\n'));
}


function quadraticEquationCalculate(inputPath, outputPath){
    let data = read(inputPath);
    let results = [];
    for(let i=0;i<data.length;++i){
        let args = data[i].split(' ');
        results.push(quadratic_equation(+args[0], +args[1], +args[2]));
    }
    results = results.map(pair=>pair = pair.join(' '));
    results.push('');
    write(outputPath, results);
}

function quadraticEquationTest(outputPath, expectedPath, resultPath){
    let output = read(outputPath);
    let expected = read(expectedPath);
    let result = [];
    let passed = 0;

    for(let i=0;i<output.length;++i){
        if(output[i]!==expected[i]){
            result.push(`Line ${i+1}: Output - ${output[i]}, Expected - ${expected[i]}`);
        }else{
            ++passed;
        }
    }
    result.push(`${passed/output.length*100}% of tests was passed`);
    write(resultPath, result);
}


function main(){
	quadraticEquationCalculate('input.txt', 'output.txt');
	quadraticEquationTest('output.txt', 'expected.txt', 'result.txt');
}

main();


