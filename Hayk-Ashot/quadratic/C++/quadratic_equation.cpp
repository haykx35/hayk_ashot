#include <vector>
#include "File.h"
#include <cmath>
#include "InfiniteSolutionsException.h"
#include "NoSolutionsException.h"
#include "NoRealSolutionsException.h"


double linear_equation(double a, double b) {
    if (a == 0 && b == 0) {
        throw InfiniteSolutionsException("X c R");
    }
    if (a == 0) {
        throw NoSolutionsException("X c O");
    }
    return -b / a;
}

double discriminant(int a, int b, int c) {
    return b * b - 4 * a * c;
}

std::pair<double, double> quadratic_equation(int a, int b, int c) {
    if (a == 0) {
        std::string state;
        double solution{};
        try {
            solution = linear_equation(b, c);
        }
        catch (const NoSolutionsException &e) {
            throw NoSolutionsException(e.what());
        }
        catch (const InfiniteSolutionsException &e) {
            throw InfiniteSolutionsException(e.what());
        }
        return {solution, solution};
    }

    double d = discriminant(a, b, c);
    if (d == 0) {
        return std::make_pair(-b / (2 * a), -b / (2 * a));
    }
    if (d > 0) {
        return std::make_pair((-b + std::pow(d, 0.5)) / (2 * a), (-b - std::pow(d, 0.5)) / (2 * a));
    }
    throw NoRealSolutionsException("X c I");
}


std::vector<std::pair<std::pair<double, double>, std::string>>
calculateQuadraticEquation(const std::vector<std::vector<double>> &trios) {
    std::vector<std::pair<std::pair<double, double>, std::string>> results;
    for (auto trio: trios) {
        std::string state;
        std::pair<double, double> solution{};
        try {
            solution = quadratic_equation(trio[0], trio[1], trio[2]);
        }
        catch (const NoSolutionsException &e) {
            state = e.what();
        }
        catch (const InfiniteSolutionsException &e) {
            state = e.what();
        }
        catch (const NoRealSolutionsException &e) {
            state = e.what();
        }
        results.emplace_back(solution, state);
    }
    return results;
}


int main() {
    std::vector<std::vector<double>> trios = File::readFromFile("input.txt");
    std::vector<std::pair<std::pair<double, double>, std::string>> results = calculateQuadraticEquation(trios);
    File::writeInFile("output.txt", results);
    File::compare("output.txt", "expected.txt", "result.txt");
    return 0;
}
