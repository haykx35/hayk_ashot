#include "File.h"

std::vector<std::vector<double>>
File::readFromFile(const std::string &path) {
    std::vector<std::vector<double>> trios;
    std::ifstream file;
    file.open(path);
    while (file.good()) {
        std::vector<double> trio(3);
        file >> trio[0];
        file >> trio[1];
        file >> trio[2];
        trios.push_back(trio);
    }
    file.close();
    return trios;
}

void
File::writeInFile(const std::string &path, std::vector<std::pair<std::pair<double, double>, std::string>> &results) {
    std::ofstream file;
    file.open(path);
    for (int i = 0; i < results.size() - 1; ++i) {
        if (results[i].second.empty()) {
            file << results[i].first.first << ' ';
            file << results[i].first.second << '\n';
        } else {
            file << results[i].second << '\n';
        }
    }

    std::pair<std::pair<double, double>, std::string> last = results[results.size() - 1];

    if (last.second.empty()) {
        file << last.first.first << ' ';
        file << last.first.second;
    } else {
        file << last.second;
    }

    file.close();
}

void
File::compare(const std::string &path1, const std::string &path2, const std::string &path3) {
    std::ifstream output(path1);
    std::ifstream expected(path2);
    std::ofstream result(path3);
    std::string line1;
    std::string line2;
    std::getline(output, line1);
    std::getline(expected, line2);
    int lineNumber = 1;
    int incorrect = 0;
    while (!line1.empty() && !line2.empty()) {
        if (line1 != line2) {
            ++incorrect;
            result << "Line: " << lineNumber << ". Output - " << line1 << ", Expected - " << line2 << '\n';
        }
        line1.clear();
        line2.clear();
        std::getline(output, line1);
        std::getline(expected, line2);
        ++lineNumber;
    }

    result << (lineNumber - 1 - incorrect) / (double) (lineNumber - 1) * 100 << "% of tests was passed";

    output.close();
    expected.close();
    result.close();
}
