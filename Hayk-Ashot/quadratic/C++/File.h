#ifndef FILE_H
#define FILE_H

#include <vector>
#include <fstream>


class File{
public:
    static std::vector<std::vector<double>> readFromFile(const std::string&);

    static void writeInFile(const std::string &path, std::vector<std::pair<std::pair<double, double>, std::string>>&);

    static void compare(const std::string &, const std::string &, const std::string &);
};

#endif //FILE_H
