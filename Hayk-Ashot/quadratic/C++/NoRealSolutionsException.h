#ifndef NOREALSOLUTIONSEXCEPTION_H
#define NOREALSOLUTIONSEXCEPTION_H

#include <exception>

class NoRealSolutionsException:public std::exception{
public:
    explicit NoRealSolutionsException(const char *message) : message(message) {}

    const char *what() const

    noexcept override{
        return message;
    }
private:
    const char *message;
};


#endif //NOREALSOLUTIONSEXCEPTION_H
