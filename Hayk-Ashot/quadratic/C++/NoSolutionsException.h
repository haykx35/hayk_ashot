#ifndef NOSOLUTIONSEXCEPTION_H
#define NOSOLUTIONSEXCEPTION_H

#include <exception>

class NoSolutionsException : public std::exception {
public:
    explicit NoSolutionsException(const char *message) : message(message) {}

    const char *what() const

    noexcept override{
        return message;
    }
private:
    const char *message;
};


#endif //NOSOLUTIONSEXCEPTION_H
