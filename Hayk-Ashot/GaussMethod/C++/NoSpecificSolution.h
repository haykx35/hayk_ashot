//
// Created by User on 7/22/2022.
//

#ifndef GAUSS_ELIMINATION_METHOD_NOSPECIFICSOLUTION_H
#define GAUSS_ELIMINATION_METHOD_NOSPECIFICSOLUTION_H

#include <exception>
;

class NoSpecificSolution : public std::exception {
public:
    explicit NoSpecificSolution(const char *message) : std::exception(message) {}
};

#endif //GAUSS_ELIMINATION_METHOD_NOSPECIFICSOLUTION_H
