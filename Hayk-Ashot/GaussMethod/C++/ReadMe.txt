////A program that solves a system of linear equations using the Gauss method////

The program read write and compar due to c++ <fstream> library
The program reads the necessary information from test1.input (the file name can be changed in main.cpp),
solves the problem and using the solve function writes the results to test1.output (the file name can be changed in main.cpp),
and compares with the true result which is stored in  test1.expected.

To run test type *make qa*
To only run use *make* or *make run* 

////Necessary parameters for the program to work////
1.Parameters that should be written in the test1.input initially
    1.1 Number of rows(parrameters(x1 x2 x3....xN )).
    1.2 Epsilone
    1.3 Matrix 
    1.4 Free Variables
    Golden example is current test1.input
2.tes1.expected file avelability

////Matrix class////
The Matrix class has only one data member and some member functions and it  gives minimal interface to use objects of Matrix type in the Gauss method's implementation.

////IndexException class////
The IndexException class extends from std::exception class. It gives opportunity to throw an exception when an invalid index was passed to functions as parameter.

THANK YOU
