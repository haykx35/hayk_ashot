#include <iostream>
#include <vector>
#include <fstream>
#include <execution>
#include <cassert>
#include "Matrix.hpp"

void validate(std::vector<double>& result);

Matrix
read_from_file(const std::string &filename) {
    std::vector<std::vector<double>> vectors;
    int numberOfEquations;
    std::ifstream file;
    file.open(filename);
    if (file.fail()) {
        //throw FileNotFoundException("File not found");
        assert(false);
    }
    file >> numberOfEquations;
    if (numberOfEquations < 0) {
        //throw InvalidInputFileException("Invalid input");
        assert(false);
    }    
    vectors.resize(numberOfEquations);
    double val;
    for (auto &vector: vectors) {
        for (int j = 0; j < numberOfEquations + 1; ++j) {
            assert(file);
            file >> val;
            vector.push_back(val);
        }
    }
    Matrix matrix(vectors);
    file.close();
    return matrix;
}

std::vector<double>
backward_substitution(const Matrix &matrix) {
    double freeVariable = matrix[matrix.count_of_rows() - 1][matrix.count_of_columns() - 1];
    double coefficient = matrix[matrix.count_of_rows() - 1][matrix.count_of_columns() - 2];

    if (coefficient == 0 && freeVariable == 0) {
        //throw NoSpecificSolution("There is a set of solutions");
        assert(false);
    }
    if (coefficient == 0) {
        //throw NoSpecificSolution("There is no solution");
        assert(false);
    }

    std::vector<double> solution;
    int size = (int) matrix.count_of_rows();

    for (int i = size - 1; i >= 0; --i) {
        double sum = 0;
        for (int j = 0; j < solution.size(); ++j) {
            sum += matrix[i][size - j - 1] * solution[j];
        }
        solution.push_back((matrix[i][size] - sum) / matrix[i][i]);
    }
    validate(solution);
    return solution;
}


void
comparing_output_and_expected(std::string outputFilename, std::string expectedFilename, std::string resultfile) 
{    
    std::ifstream output;
    std::ifstream expected;
    std::ofstream result;
    output.open(outputFilename);
    expected.open(expectedFilename);
    result.open(resultfile);
    std::string str1;
    std::string str2;
    std::getline(output,str1);
    std::getline(expected,str2);
    while(!str1.empty() && !str2.empty()){
        if(str1 != str2) {
            result << str1 << " " << str2 << " //FAIL//" << std::endl;
            return;
        }
        result << str1 << " " << str2 << " //PASS//" << std::endl;
        str1.clear();
        str2.clear();
        std::getline(output,str1);
        std::getline(expected,str2);
    }
    output.close();
    expected.close();
    result.close();
}


void
write_into_file(std::vector<double> &&result, const std::string &filename) {
    std::ofstream file;
    file.open(filename);
    for (int i = 0; i < result.size(); ++i) {
        file << "x[" << i + 1 << "] = " << result[i] << "\n";
    }

    file.close();
}

Matrix
transform_to_upper_triangular(Matrix matrix, int lineIndex = 1) {
    if (lineIndex == matrix.count_of_rows()) {
        return matrix;
    }
    size_t size = matrix.count_of_rows();

    for (int i = lineIndex; i < size; ++i) {
        double coefficient = matrix[i][lineIndex - 1] / matrix[lineIndex - 1][lineIndex - 1];
        std::vector<double> tempRow = matrix.get_row(lineIndex - 1);
        std::vector<double> newRow = matrix.get_row(i);
        for (int j = 0; j < tempRow.size(); ++j) {
            newRow[j] -= coefficient * tempRow[j];
        }
        matrix.set_row(newRow, i);
    }
    ++lineIndex;
    return transform_to_upper_triangular(matrix, lineIndex);
}

void 
validate(std::vector<double>& result)
{
    for(int i = 0; i < result.size()/2;++i) {
        if(result[i] == -0) {
            result[i] = std::abs(result[i]);
        }
        std::swap(result[i],result[result.size()-i-1]);
    } 
}

std::vector<double>
solve(const Matrix &matrix) {
    return backward_substitution(transform_to_upper_triangular(matrix));
}

int main() 
{
    
    write_into_file(solve(read_from_file("test1.input")), "test1.output");
    comparing_output_and_expected("test1.output", "test1.expected", "test1.result");
    return 0;
}
