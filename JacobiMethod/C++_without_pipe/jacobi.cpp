#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <cassert>
#include "Matrix.hpp"

/*The function checks whether the currect error corresponds to the specified*/
/*Checking is done by comparing two results, current and previous*/
bool 
almostSame(const std::vector<double> &a, const std::vector<double> &b,double eps)
{
    if (a.size() != b.size()) {
        return false;
    }
    for (int i = 0; i < a.size(); ++i) {
        if (std::abs(a[i] - b[i]) > eps) {
            return false;
        }
    }
    return true;
}

/*Major function that calculate solution*/
std::vector<double> 
solve(const Matrix &matrix, const std::vector<double> &B, double eps) 
{
    if (!matrix.is_diagonally_dominant()) {
        std::cout << "Matrix is not diagonally dominant\n";
        std::vector<double> noSol;
        return noSol;
    }

    std::vector<double> sol(matrix.count_of_rows(), 0);
    std::vector<double> tempSol(matrix.count_of_rows(), eps + 1);//this initialization is for first iteration(sol=tempSol)

    while (!almostSame(sol, tempSol, eps)) {
        sol = tempSol;
        tempSol.clear();
        for (int i = 0; i < matrix.count_of_rows(); ++i) {
            double sum = 0;
            for (int j = 0; j < matrix[i].size(); ++j) {
                sum += matrix[i][j] * sol[j];
            }
            tempSol.push_back((B[i] - (sum - matrix[i][i] * sol[i])) / matrix[i][i]);
        }
    }
    return sol;
}

void 
read_from_file(Matrix& matrix, std::vector<double>& freeVariables, double& eps, const std::string& filename)
{
    std::vector<std::vector<double>> vectors;
    int numberOfEquations;
    std::ifstream file;
    file.open(filename);
    assert(file);
    file >> numberOfEquations;
    file >> eps;
    assert(file);
    vectors.resize(numberOfEquations);
    double val;
    for(int i = 0; i < vectors.size(); ++i) {
        for(int j = 0; j < numberOfEquations; ++j) {
            assert(file);
            file >> val;
            vectors[i].push_back(val);
        }
    }
    matrix = vectors;
    for(int i = 0; i < numberOfEquations; ++i) {
        assert(file);
        file >> val;
        freeVariables.push_back(val);
    }
    file.close();
}

void
comparing_output_and_expected(std::string outputFilename, std::string expectedFilename, std::string resultfile) 
{    
    std::ifstream output;
    std::ifstream expected;
    std::ofstream result;
    output.open(outputFilename);
    expected.open(expectedFilename);
    result.open(resultfile);
    std::string str1;
    std::string str2;
    std::getline(output,str1);
    std::getline(expected,str2);
    while(!str1.empty() && !str2.empty()){
        if(str1 != str2) {
            result << str1 << " " << str2 << " //FAIL//" << std::endl;
            return;
        }
        result << str1 << " " << str2 << " //PASS//" << std::endl;
        str1.clear();
        str2.clear();
        std::getline(output,str1);
        std::getline(expected,str2);
    }
    output.close();
    expected.close();
}

void 
write_into_file(std::vector<double>& result, std::string filename) 
{
    std::ofstream file;
    file.open(filename);
    for(int i = 0; i < result.size(); ++i) {
        file << "x[" << i+1 << "] = " << result[i] << "\n";
    }
    file.close();
}

/* Main function that crating nessesary variables and serves for start point for solving*/
int 
main() 
{
    Matrix matrix;
    std::vector<double> freeVariables;
    double eps;
    std::string inputFile = "test1.input";
    read_from_file(matrix,freeVariables,eps,inputFile);

    std::vector<double> solution = solve(matrix, freeVariables, eps);

    std::string outputFile = "test1.output";
    write_into_file(solution,outputFile);
    std::string expectedFilename = "test1.expected";
    std::string resultFilename = "test1.result";

    comparing_output_and_expected(outputFile,expectedFilename,resultFilename);
    return 0;
}
