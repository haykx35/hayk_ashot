#include "Matrix.hpp"
#include <utility>
#include <vector>
#include <cassert>

Matrix::Matrix(size_t rows, size_t columns) {
    table.resize(rows);
    for (auto &row: table) {
        row.resize(columns);
    }
}

Matrix::Matrix(std::vector<std::vector<double>> matrix) : table(matrix) {}

const std::vector<double> &
Matrix::get_row(size_t row_index) const {
    check_row_index(row_index);
    return table[row_index];
}

Matrix&
Matrix::operator=(const Matrix& rhv){
    table = rhv.table;
    return *this;
}
Matrix&
Matrix::operator=(const std::vector<std::vector<double> >& rhvt){
    table = rhvt;
    return *this;
}

std::vector<double>
Matrix::get_column(size_t column_index){
    check_column_index(column_index);
    std::vector<double> columnsVec;
    for (const auto &row: table) {
        columnsVec.push_back(row[column_index]);
    }
    return columnsVec;
}

void
Matrix::set_row(const std::vector<double> &newRow, size_t row_index) {
    check_row_index(row_index);
    if(newRow.size()!=table[0].size()){
        return;
    }
    table[row_index] = newRow;

}

void
Matrix::set_column(const std::vector<double> &newColumn, size_t column_index) {
    check_column_index(column_index);
    if(newColumn.size()!=table.size()){
        return;
    }
    for (size_t i = 0; i < table.size(); ++i) {
        table[i][column_index] = newColumn[i];
    }
}

size_t
Matrix::count_of_rows() const {
    return table.size();
}

size_t
Matrix::count_of_columns() const {
    return !table.empty() ? table[0].size() : 0;
}

bool
Matrix::is_square() const {
    return count_of_rows() == count_of_columns();
}

bool
Matrix::is_diagonally_dominant() const {
    bool strictly = false;
    for (size_t i = 0; i < table.size(); ++i) {
        double sum = 0;
        for (double j: table[i]) {
            sum += abs(j);
        }
        if (sum < 2 * table[i][i]) {
            strictly = true;
        } else if (sum > 2 * table[i][i]) {
            return false;
        }
    }
    return strictly;
}

const std::vector<double> &
Matrix::operator[](size_t row_index) const {
    return get_row(row_index);
}

std::vector<double> &
Matrix::operator[](size_t row_index) {
    check_row_index(row_index);
    return table[row_index];
}

void
Matrix::print() const {
    for (const auto &row: table) {
        for (auto element: row) {
            std::cout << element << ' ';
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}


void
Matrix::check_row_index(size_t row_index) const {
    assert(row_index > 0 || row_index <= count_of_rows());
}

void
Matrix::check_column_index(size_t column_index) const {
    assert(column_index > 0 || column_index <= count_of_columns());
}
