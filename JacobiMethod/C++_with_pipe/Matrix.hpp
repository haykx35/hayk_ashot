//
// Created by User on 7/20/2022.
//

#ifndef JACOBI_ITERATION_METHOD_MATRIX_H
#define JACOBI_ITERATION_METHOD_MATRIX_H
#include <iostream>
#include <vector>

///Class that gives abstraction of 2D arrays(matrices)
class Matrix {
public:
    //Initializer/Default constructor
    explicit Matrix(size_t = 0, size_t = 0);

    //Initializer constructor
    explicit Matrix(std::vector<std::vector<double>>);

    //Disallow copying
    Matrix(const Matrix &) = delete;

    //Assignment operator 1
    Matrix& operator=(const Matrix &);

    //Assignment operator 2
    Matrix& operator=(const std::vector<std::vector<double> >&);

    //Returns required row of table
    const std::vector<double> &get_row(size_t) const;

    //Returns required column of table
    std::vector<double> get_column(size_t);

    //Alters required row of table
    void set_row(const std::vector<double> &, size_t);

    //Alters required column of table
    void set_column(const std::vector<double> &, size_t);

    //Returns count of rows
    size_t count_of_rows() const;

    //Returns count of columns
    size_t count_of_columns() const;

    //Checks the squareness
    bool is_square() const;

    //Checks whether table is diagonally dominant or not
    bool is_diagonally_dominant() const;

    //Return required row of table using [] brackets (for const and non-const objects)
    const std::vector<double> &operator[](size_t) const;

    //Return required row of table using [] brackets (only for non-const objects)
    std::vector<double> &operator[](size_t);

    //MatrixForm printing
    void print() const;

private:
    //Checks validity for row index
    void check_row_index(size_t)const;

    //Checks validity of column index
    void check_column_index(size_t)const;
private:
    std::vector<std::vector<double>> table;
};

#endif //JACOBI_ITERATION_METHOD_MATRIX_H
