#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <cassert>
#include <unistd.h>
#include "Matrix.hpp"

/*The function checks whether the currect error corresponds to the specified*/
/*Checking is done by comparing two results, current and previous*/
bool 
almostSame(const std::vector<double> &a, const std::vector<double> &b,double eps)
{
    if (a.size() != b.size()) {
        return false;
    }
    for (size_t i = 0; i < a.size(); ++i) {
        if (std::abs(a[i] - b[i]) > eps) {
            return false;
        }
    }
    return true;
}

/*Major function that calculate solution*/
std::vector<double> 
solve(const Matrix &matrix, const std::vector<double> &B, double eps) 
{
    if (!matrix.is_diagonally_dominant()) {
        std::cout << "Matrix is not diagonally dominant\n";
        std::vector<double> noSol;
        return noSol;
    }

    std::vector<double> sol(matrix.count_of_rows(), 0);
    std::vector<double> tempSol(matrix.count_of_rows(), eps + 1);//this initialization is for first iteration(sol=tempSol)

    while (!almostSame(sol, tempSol, eps)) {
        sol = tempSol;
        tempSol.clear();
        for (size_t i = 0; i < matrix.count_of_rows(); ++i) {
            double sum = 0;
            for (size_t j = 0; j < matrix[i].size(); ++j) {
                sum += matrix[i][j] * sol[j];
            }
            tempSol.push_back((B[i] - (sum - matrix[i][i] * sol[i])) / matrix[i][i]);
        }
    }
    return sol;
}

void 
printSol(const std::vector<double>& result) 
{
    for (size_t i = 0; i < result.size(); ++i) {
        std::cout << "x[" << i+1 << "] = " << result[i] << std::endl;
    }
}


void read(Matrix& matrix, double& eps, std::vector<double>& freeVariables )
{
   if(::isatty(STDIN_FILENO)) {
        std::cout << "Enter number of nuknowns: ";
    }
    int n;
    std::cin >> n;
    if(::isatty(STDIN_FILENO)) {
        std::cout << "Enter tolerable error: ";
    }
    std::cin >> eps;
     std::vector<std::vector<double>> vectors(n);/*Creating Matrix where will the variables be stored*/
    if(::isatty(STDIN_FILENO)) {
        std::cout << "Enter Coefficients of Augmented Matrix: " << std::endl;
    }
    /*Input Matrix and Free members*/
    double val;
	for(size_t i = 0;i < vectors.size(); ++i) {
	    for(int j = 0;j < n;++j) {
            if(::isatty(STDIN_FILENO)) {
                std::cout << "X["<< i+1 <<"]" << j+1 << "]= ";
            }
            std::cin >> val;
            vectors[i].push_back(val);
	    }
	}
    matrix = vectors; /// using operator=(const std::vector<std::vector<double> >&)
    for(int i = 0; i < n; ++i) {
        std::cin >> val;
        freeVariables.push_back(val);
    }
}

/* Main function that crating nessesary variables and serves for start point for solving*/
int 
main() 
{
    Matrix matrix;
    std::vector<double> freeVariables;
    double eps;
    read(matrix,eps,freeVariables);
    printSol(solve(matrix, freeVariables, eps));
    return 0;
}
